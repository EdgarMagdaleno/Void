var fs = require('fs');
var express = require('express');
var https = require('https');
var app = express();

var sslPath = '/etc/letsencrypt/live/edgarmagdaleno.me/';
var options = {
	ca: fs.readFileSync(sslPath + 'chain.pem'),
	key: fs.readFileSync(sslPath + 'privkey.pem'),
	cert: fs.readFileSync(sslPath + 'cert.pem')
};

app.get('/', function(request, response) {
	response.sendFile(__dirname + '/index.html');
});

var io = require('socket.io')(https.createServer(options, app).listen(4000));

var players = {};
var clients = {};

var Player = function(x, y) {
	this.x = x;
	this.y = y;
	this.name = "";
};

io.sockets.on('connection', function(socket) {
	var x, y;
	do {
		x = Math.round(Math.random() * 630);
		y = Math.round(Math.random() * 470);
	}while(x % 10 != 0 || y % 10 != 0);

	var player = new Player(x, y);
	players[socket.id] = player;
	clients[socket.id] = socket;

	socket.emit('start', {
		id : socket.id, 
		playerList : players
	});

	socket.on('playerUpdate', function(player) {
		players[socket.id] = player;
	});

	socket.on('disconnect', function() {
		delete players[socket.id];
		delete clients[socket.id];
	});
});

setInterval(send, 32);

function send() {
	for(var i in clients)
		clients[i].emit('update', players);
}